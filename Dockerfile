# build stage
FROM node:9.11.1-alpine AS build-stage
LABEL MAINTAINER Augusto Silva <augusto.t.silva1993@gmail.com>
# Create Directory for the Container
WORKDIR /
# Only copy the package.json file to work directory
COPY package.json .
# Install all Packages
RUN npm install

COPY . .

RUN npm run build
# Copy all other source code to work directory

# TypeScript
# Start
CMD [ "npm", "run", "dev" ]
EXPOSE 3001