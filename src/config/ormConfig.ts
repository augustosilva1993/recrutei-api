import * as dotenv from 'dotenv';
import { ConnectionOptions } from "typeorm";

dotenv.config({ path: '.env'});

export const connectionOptions: ConnectionOptions = {
  type: "mysql",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT) || 3306,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: true,
  migrationsRun: true,
  logging: true,
  entities: [
    "src/entity/*.{.ts,.js}", "src/entity/*.{.ts,.js}", "dist/entity/*.js"
  ],
  subscribers: [
    "src/subscriber/*.{.ts,.js}", "src/subscriber/*.{.ts,.js}"
  ],
  migrations: [
    "src/migration/*.{.ts,.js}", "src/migration/*.{.ts,.js}", "dist/**/migration/*.{.ts,.js}"
  ],
  cli: {
    entitiesDir: "src/entity",
    migrationsDir: "src/migration",
    subscribersDir: "src/subscriber"
  }
};