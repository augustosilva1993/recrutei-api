import { Request, Response } from "express";
import { checkRole } from "../../middleware/checkRole"
import { WorkOrderHistoryController } from "./WorkOrderHistoryController";

export default [
  {
    path: "/api/v1/work-order-history",
    method: "post",
    handler: [
      checkRole(["HELP-DESK", "ADMIN", "DEVELOPER"]),
      async ({ body }: Request, res: Response) => {
        const result = await new WorkOrderHistoryController().save(body);
        res.status(200).send(result);
      }
    ]
  },  
  {
    path: "/api/v1/work-order-history",    
    method: "put",
    handler: [
      checkRole(["ADMIN"]),
      async ({ body }: Request, res: Response) => {
        const result = await new WorkOrderHistoryController().edit(body);
        res.status(200).send(result);
      }
    ]
  },  
  {
    path: "/api/v1/work-order-history/:id([0-9]+)",
    method: "delete",
    handler: [
      checkRole(["ADMIN"]),
      async ({ params }: Request, res: Response) => {
        const result = await new WorkOrderHistoryController().remove(Number(params.id));
        res.status(200).send(result);
      }
    ]
  },        
  {
    path: "/api/v1/work-order-history",
    method: "get",
    handler: [
      async ({}: Request, res: Response) => {
        const result = await new WorkOrderHistoryController().list();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/work-order-history/:idWorkOrder([0-9]+)",
    method: "get",
    handler: [
      async ( { params } : Request, res: Response) => {
        const result = await new WorkOrderHistoryController().getOne(Number(params.idWorkOrder));
        res.status(200).send(result);
      }
    ]
  }
]