import { WorkOrderHistory } from "../../entity/WorkOrderHistory";
import { getRepository } from "typeorm";
import { ApiResponse } from "../../domain/ApiResponse";

class WorkOrderHistoryController {

  async save(workOrder: WorkOrderHistory) {
    const repository = getRepository(WorkOrderHistory);
    let response: ApiResponse = new ApiResponse();
    
    await repository.save(workOrder).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
    });
    
    return response;
  }

  async edit(workOrder: WorkOrderHistory) {
    const repository = getRepository(WorkOrderHistory);
    let response: ApiResponse = new ApiResponse();
    
    await repository.save(workOrder).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
    });
    
    return response;
  }

  async remove(id: number) {
    const repository = getRepository(WorkOrderHistory);
    let response: ApiResponse = new ApiResponse();
    
    await repository.delete(id).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
    });
    
    return response;
  }

  async getOne(idWorkOrder: number) {
    const repository = getRepository(WorkOrderHistory);
    const response: ApiResponse = new ApiResponse();
    
    const result = await repository.find({ where: { workOrderId: idWorkOrder }});
    response.data = Object.assign([], result);
    response.success = true;
    
    return response;
  }

  async list() {
    const repository = getRepository(WorkOrderHistory);
    let response: ApiResponse = new ApiResponse();
    
    await repository.find().catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
      response.data = Object.assign([], value);
    });
    
    return response;
  }
}

export { WorkOrderHistoryController };