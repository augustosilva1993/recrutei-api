import { Request, Response } from "express";
import { checkRole } from "../../middleware/checkRole"
import { WorkOrderController } from "./WorkOrderController";

export default [
  {
    path: "/api/v1/work-order",
    method: "post",
    handler: [
      checkRole(["HELP-DESK", "ADMIN"]),
      async ({ body }: Request, res: Response) => {
        const result = await new WorkOrderController().save(body);
        res.status(200).send(result);
      }
    ]
  },  
  {
    path: "/api/v1/work-order/:id([0-9]+)",    
    method: "put",
    handler: [
      checkRole(["ADMIN"]),
      async ({ params, body }: Request, res: Response) => {
        const result = await new WorkOrderController().edit(Number(params.id), body);
        res.status(200).send(result);
      }
    ]
  },  
  {
    path: "/api/v1/work-order/:id([0-9]+)",
    method: "delete",
    handler: [
      checkRole(["ADMIN"]),
      async ({ params }: Request, res: Response) => {
        const result = await new WorkOrderController().remove(Number(params.id));
        res.status(200).send(result);
      }
    ]
  },        
  {
    path: "/api/v1/work-order",
    method: "get",
    handler: [
      async ({}: Request, res: Response) => {
        const result = await new WorkOrderController().list();
        res.status(200).send(result);
      }
    ]
  },
  {
    path: "/api/v1/work-order/:id",
    method: "get",
    handler: [
      async ({ params }: Request, res: Response) => {
        const result = await new WorkOrderController().getOne(Number(params.id));
        res.status(200).send(result);
      }
    ]
  }
]