import { getRepository } from "typeorm";
import { WorkOrder } from "../../entity/WorkOrder";
import { ApiResponse } from "../../domain/ApiResponse";

class WorkOrderController {

  async save(workOrder: WorkOrder) {
    const repository = getRepository(WorkOrder);
    let response: ApiResponse = new ApiResponse();
    
    await repository.save(workOrder).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
    });
    
    return response;
  }

  async edit(id:number, workOrder: WorkOrder) {
    const repository = getRepository(WorkOrder);
    let response: ApiResponse = new ApiResponse();
    
    await repository.update(id, workOrder).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
    });
    
    return response;
  }

  async remove(id: number) {
    const repository = getRepository(WorkOrder);
    let response: ApiResponse = new ApiResponse();
    
    await repository.delete(id).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
    });
    
    return response;
  }

  async getOne(id: number) {
    const repository = getRepository(WorkOrder);
    const response: ApiResponse = new ApiResponse();
    
    await repository.findByIds([id]).catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
      response.data = Object.assign([], value);
    });
    return response;
  }

  async list() {
    const repository = getRepository(WorkOrder);
    let response: ApiResponse = new ApiResponse();
    
    await repository.find().catch(err => {
      response.success = false;
      response.message = err;        
    }).then(value => {
      response.success = true;
      response.data = Object.assign([], value);
    });
    
    return response;
  }
}

export { WorkOrderController };