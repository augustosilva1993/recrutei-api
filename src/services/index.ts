import workOrder from "./workorder/routes";
import workOrderHistory from "./workorder-history/routes";

export default [...workOrder, ...workOrderHistory];