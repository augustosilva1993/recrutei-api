import {MigrationInterface, QueryRunner, Table, TableIndex, TableColumn, TableForeignKey } from "typeorm";

export class InitialDB implements MigrationInterface {
    
    async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
          name: "WorkOrder",
          columns: [
            {
              name: "id",
              type: "int",
              isPrimary: true
            },
            {
              name: "title",
              type: "varchar",
            },
            {
              name: "description",
              type: "varchar",
            },
            {
              name: "created_at",
              type: "datetime",
            },
            {
              name: "updated_at",
              type: "datetime",
            }                
          ]
        }), true)        
      
        await queryRunner.createTable(new Table({
          name: "WorkOrderHistory",
          columns: [
            {
              name: "id",
              type: "int",
              isPrimary: true
            },
            {
              name: "idWorkOrder",
              type: "int",
            },
            {
              name: "idWorkOrder",
              type: "varchar",
            },
            {
              name: "notes",
              type: "varchar",
            },
            {
              name: "created_at",
              type: "datetime",
            },
            {
              name: "updated_at",
              type: "datetime",
            }                              
          ]
      }), true);        
        
      await queryRunner.createForeignKey("WorkOrderHistory", new TableForeignKey({
        columnNames: ["idWorkOrder"],
        referencedColumnNames: ["id"],
        referencedTableName: "WorkOrder",
        onDelete: "CASCADE"
      }));
    }

    async down(queryRunner: QueryRunner): Promise<any> {

    }
}