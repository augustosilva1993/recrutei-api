import { Column, Entity, PrimaryGeneratedColumn, ObjectIdColumn, Timestamp } from "typeorm";
import { WorkOrderHistory } from "./WorkOrderHistory";

@Entity()
export class WorkOrder {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
  updated_at: Date;
}