import {Column, Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { WorkOrder } from "./WorkOrder";

@Entity()
export class WorkOrderHistory {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => WorkOrder, { onDelete: 'CASCADE' })
  @JoinColumn()
  workOrder: WorkOrder;

  @Column()
  notes: string;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
  updated_at: Date;
}