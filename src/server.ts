import http from "http";
import express from "express";
import { applyMiddleware, applyRoutes } from "./utils";
import middleware from "./middleware";
import errorHandlers from "./middleware/errorHandlers";
import routes from "./services";
import { createConnection } from "typeorm";
import { connectionOptions } from "./config/ormConfig"

const router = express();
applyMiddleware(middleware, router);
applyRoutes(routes, router);
applyMiddleware(errorHandlers, router);

createConnection(connectionOptions).then(async connection => {
  console.log("Connected to DB");
}).catch(error => console.log("TypeORM connection error: ", error));

const { PORT = 3000 } = process.env;
const server = http.createServer(router);

process.on("uncaughtException", e => {
  console.log(e);
  process.exit(1);
});process.on("unhandledRejection", e => {
  console.log(e);
  process.exit(1);
});

server.listen(PORT, () =>
  console.log(`Server is running http://localhost:${PORT}...`)
);