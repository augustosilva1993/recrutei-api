export class ApiResponse {
  success: boolean;
  message: string;
  data: Array<any>[];
}