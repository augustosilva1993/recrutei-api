import { Request, Response, NextFunction } from "express";

export const checkRole = (roles: Array<string>) => {
  return async ({ headers }: Request, res: Response, next: NextFunction) => {
    let role: string;
    
    role = headers["access-role"] as string;
    
    if (roles.indexOf(role) > -1) {
      next();
    } else {
      res.status(401).send();
    }
  };
};