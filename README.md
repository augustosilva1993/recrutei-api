# Recrutei API

## Descrição do Serviço

Este serviço tem como finalidade manter os registros de chamados do cliente abertos pelo Service Desk.

* Um usuário com perfil "Service-Desk" deve ter permissão para criar uma Ordem de Serviço (OS) e consultar suas alterações.

* Um usuário com perfil "Developer" deve ter permissão para adicionar "notas" (OS Histórico) a OS.

* Um usuário com perfil "Admin" deve ter permissão para editar e excluir OS e Histórico.

## Tecnologias

A API foi desenvolvida utilizando TypeScript (ES6) e Express. Como banco de dados está sendo utilizado o MySQL mantido em RemoteMySql.com. Para o gerenciamento do banco de dados foi utilizado o TypeORM.

### CI/CD do Bitbucket

Criação de pipeline no Bitbucket integrada ao Docker HUB para geração de imagens. Pode-se aprofundar e implementar a execução de testes unitários, validação do Dockerfile e entrega em diferentes ambientes após aprovações.

### PM2

A aplicação está utilizando o PM2 como gerenciador de processos. Essa ferramenta permite gerenciar os processos e escalar (na mesma máquina) rapidamente.

## Possíveis melhorias

### Cache

As Ordens de Serviço recém criadas (a depender do tempo de resposta do setor responsável) podem ser colocadas em um banco de dados de cache como Redis para evitar consumo de recursos do banco de dados.

### PM2

Com vista atender a um maior número de usuários pode ser aumentado o número de instâncias rodando.

### Utilização de Filas

Deve ser utilizado caso sejam realizadas algumas integrações como envio de notificações por e-mail, geração de relatórios por um CRM etc.

## Utilização

### Docker

A imagem do docker contendo a aplicação está disponível no Docker HUB.

Para rodar é necessário executar o comando:

`docker run -it -p 3001:3001 --rm --name recrutei-api augustotsilva1993/recrutei-api:3eb59f3bdc351e853d4481d508d3559bd4d8c8fc`

### Projeto

O projeto pode ser clonado do repositório e executado com os comandos

`npm install` 

e 

`npm run dev`

As configurações podem ser alteradas no .env